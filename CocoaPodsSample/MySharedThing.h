//
//  MySharedThing.h
//  CocoaPodsSample
//
//  Created by Eric Zhuang on 18/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MySharedThing : NSObject
+ (id)sharedInstance;
@end
