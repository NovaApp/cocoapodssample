//
//  MySharedThing.m
//  CocoaPodsSample
//
//  Created by Eric Zhuang on 18/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MySharedThing.h"
#import "GeneralUtility.h"

@implementation MySharedThing

+ (id)sharedInstance
{
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [[self alloc] init];
    });
}
@end
