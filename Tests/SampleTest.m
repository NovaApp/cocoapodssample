//
//  SampleTest.m
//  CocoaPodsSample
//
//  Created by Eric Zhuang on 16/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <GHUnitIOS/GHUnit.h>
@interface SampleTest: GHTestCase

@end

@implementation SampleTest

// Run before each test method
- (void)setUp { }

// Run after each test method
- (void)tearDown { }

// Run before the tests are run for this class
- (void)setUpClass { }

// Run before the tests are run for this class
- (void)tearDownClass { }

- (void) testSanityCheck {
    NSString *value = @"123";
    GHAssertNotNil(value, @"value is nil");
}

// Override any exceptions; By default exceptions are raised, causing a test failure
- (void)failWithException:(NSException *)exception { }

@end
