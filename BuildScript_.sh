#!/bin/sh

#  BuildScript.sh
#  CocoaPodsSample
#
#  Created by Eric Zhuang on 16/07/2012.
#  Copyright (c) 2012 __MyCompanyName__. All rights reserved.


RBS_TARGET_NAME="RBS"
NWB_TARGET_NAME="Natwest"

PROJECT_NAME=QLUniversalNatwest
PROJDIR=$WORKSPACE
TARGET_SDK="iphoneos"
CONFIGURATION="Ad Hoc"
PROJECT_BUILDDIR="${PROJDIR}/build/$CONFIGURATION-iphoneos"
FOLDER_NAME=$PROJECT_NAME$(date "+%d-%m-%Y-%H_%M_%S")
BUILD_HISTORY_DIR="/Users/$USER/Desktop/$FOLDER_NAME"


AFRICA_PROVISONNING_PROFILE="/Users/$USER/Library/MobileDevice/Provisioning Profiles/Monitise_Africa_Ad_Hoc.mobileprovision" #change it
AFRICA_PROFILE_NAME='iPhone Distribution: Monitise Africa (UK)Limited,C/=GB'
AFRICA_SIGN_PROVISONNING_PROFILE="$(cat /Users/$USER/Library/MobileDevice/Provisioning\ Profiles/Monitise_Africa_Ad_Hoc.mobileprovision | egrep  -a  -o '[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}')"

MEML_PROVISONNING_PROFILE="/Users/$USER/Library/MobileDevice/Provisioning Profiles/Monitise_Emerging_Markets_Ad_Hoc_Profile.mobileprovision"
MEML_PROFILE_NAME="iPhone Distribution: Monitise Emerging Markets Limited"
MEML_SIGN_PROVISONNING_PROFILE="$(cat /Users/$USER/Library/MobileDevice/Provisioning\ Profiles/Monitise_Emerging_Markets_Ad_Hoc_Profile.mobileprovision | egrep  -a  -o '[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}')"

EUROPE_PROVISONNING_PROFILE="/Users/$USER/Library/MobileDevice/Provisioning Profiles/Monitise_Europe_Ad_Hoc_Profile.mobileprovision"
EUROPE_PROFILE_NAME="iPhone Distribution: Monilink Ltd"
EROPE_SIGN_PROVISONNING_PROFILE="$(cat /Users/$USER/Library/MobileDevice/Provisioning\ Profiles/Monitise_Europe_Ad_Hoc_Profile.mobileprovision | egrep  -a  -o '[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}')"

ENTERPRISE_PROVISONNING_PROFILE="/Users/$USER/Library/MobileDevice/Provisioning Profiles/5E3F0A0C-D705-4472-A926-880FC2134422.mobileprovision"
ENTERPRISE_PROFILE_NAME="iPhone Distribution: Monitise International PLC"
ENTERPRISE_SIGN_PROVISONNING_PROFILE="$(cat /Users/$USER/Library/MobileDevice/Provisioning\ Profiles/5E3F0A0C-D705-4472-A926-880FC2134422.mobileprovision | egrep  -a  -o '[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}')"

RBS_PROVISONNING_PROFILE="/Users/$USER/Library/MobileDevice/Provisioning Profiles/DE8C9D28-3D0D-4C40-A27B-4922D621D6EE.mobileprovision"
RBS_PROFILE_NAME="iPhone Distribution: RBS"
RBS_SIGN_PROVISONNING_PROFILE="$(cat /Users/$USER/Library/MobileDevice/Provisioning\ Profiles/DE8C9D28-3D0D-4C40-A27B-4922D621D6EE.mobileprovision | egrep  -a  -o '[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}')"


MONITISE_NWB_APPSTORE_PROVISONNING_PROFILE="/Users/$USER/Library/MobileDevice/Provisioning Profiles/Monitise_NatWest_EN_GB_App_Store.mobileprovision"
MONITISE_NWB_APPSTORE_PROFILE_NAME="iPhone Distribution: Monitise International"
MONITISE_NWB_APPSTORE_SIGN_PROVISONNING_PROFILE="$(cat /Users/$USER/Library/MobileDevice/Provisioning\ Profiles/Monitise_NatWest_EN_GB_App_Store.mobileprovision | egrep  -a  -o '[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}')"


MONITISE_RBS_APPSTORE_PROVISONNING_PROFILE="/Users/$USER/Library/MobileDevice/Provisioning Profiles/Monitise_RBS_EN_GB_App_Store.mobileprovision"
MONITISE_RBS_APPSTORE_PROFILE_NAME="iPhone Distribution: Monitise International"
MONITISE_RBS_APPSTORE_SIGN_PROVISONNING_PROFILE="$(cat /Users/$USER/Library/MobileDevice/Provisioning\ Profiles/Monitise_RBS_EN_GB_App_Store.mobileprovision | egrep  -a  -o '[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}')"


export OUTPUT=$WORKSPACE/output
rm -rf $OUTPUT || echo "Cannot delete the output folder"
mkdir -p $OUTPUT

cd "$WORKSPACE"

cp -f "$WORKSPACE"/AppDelegate_iPad.m.origin.m "$WORKSPACE"/iPad/Delegates/AppDelegate_iPad.m
cp -f "$WORKSPACE"/AppDelegate_iPhone.m.origin.m "$WORKSPACE"/iPhone/Delegates/AppDelegate_iPhone.m



function patchBundleVersion()
{
    p=$(/usr/libexec/PlistBuddy -c Print:CFBundleVersion "$WORKSPACE/$1 Branding/Info.plist")".$BUILD_NUMBER"
    /usr/libexec/PlistBuddy -c "Set :CFBundleVersion $p" "$WORKSPACE/$1 Branding/Info.plist"
    /usr/libexec/PlistBuddy -c "Set :CFBundleShortVersionString $p" "$WORKSPACE/$1 Branding/Info.plist"    
    /usr/libexec/PlistBuddy -c "Set :PreferenceSpecifiers:5:DefaultValue $p" "$WORKSPACE/$1 Branding/Settings.bundle/Root.plist"    
    echo $(/usr/libexec/PlistBuddy -c "Print :PreferenceSpecifiers:5:DefaultValue" "$WORKSPACE/$1 Branding/Settings.bundle/Root.plist")
}

function build()
{
    rm -rf "$WORKSPACE/build"

    /Users/$USER/Developer/usr/bin/xcodebuild clean -configuration "$CONFIGURATION"
    echo "Building $1 $5" 


    /Users/$USER/Developer/usr/bin/xcodebuild -target $1 -sdk "${TARGET_SDK}" -configuration "$CONFIGURATION" "CODE_SIGN_IDENTITY=$2" "PROVISIONING_PROFILE=$3"    

    #Check if build succeeded
    if [ $? != 0 ]
    then
    echo "Failed to make the build"
    exit 1
    fi

    echo "App name is $OUTPUT/$1_${BUILD_NUMBER}_$5_$BUILD_ID.ipa"
    /usr/bin/xcrun -sdk iphoneos PackageApplication -v "${PROJECT_BUILDDIR}/$1.app" -o     "$OUTPUT/$1_${BUILD_NUMBER}_$5_$BUILD_ID.ipa" --sign "$2" --embed "$4"
    echo "Copying debug symblols"
    ls -ls "${PROJECT_BUILDDIR}"
    zip -r "${PROJECT_BUILDDIR}/$1.app.dSYM.zip" "${PROJECT_BUILDDIR}/$1.app.dSYM"
    cp -r "${PROJECT_BUILDDIR}/$1.app.dSYM.zip" "$OUTPUT/$1_${BUILD_NUMBER}_$5_$BUILD_ID.app.dSYM.zip"
}


#echo Making the Africa Build
#build "$RBS_TARGET_NAME" "${AFRICA_PROFILE_NAME}" "${AFRICA_SIGN_PROVISONNING_PROFILE}" "${AFRICA_PROVISONNING_PROFILE}" Africa
patchBundleVersion "$RBS_TARGET_NAME"
echo Making the MEML Build
build "$RBS_TARGET_NAME" "${MEML_PROFILE_NAME}" "${MEML_SIGN_PROVISONNING_PROFILE}" "${MEML_PROVISONNING_PROFILE}"       MEML 
echo Making the Europe Build
build "$RBS_TARGET_NAME" "${EUROPE_PROFILE_NAME}" "${EUROPE_SIGN_PROVISONNING_PROFILE}" "${EUROPE_PROVISONNING_PROFILE}" EUROPE
echo Making the Enterpise Build
build "$RBS_TARGET_NAME" "${ENTERPRISE_PROFILE_NAME}" "${ENTERPRISE_SIGN_PROVISONNING_PROFILE}" "${ENTERPRISE_PROVISONNING_PROFILE}" Enterprise

echo 'revert version changes before creating of AppStore build'
git checkout "$WORKSPACE/$RBS_TARGET_NAME Branding/Info.plist"
git checkout "$WORKSPACE/$RBS_TARGET_NAME Branding/Settings.bundle/Root.plist"

echo Making the RBS AppStore Build
build "$RBS_TARGET_NAME" "${MONITISE_RBS_APPSTORE_PROFILE_NAME}" "${MONITISE_RBS_APPSTORE_SIGN_PROVISONNING_PROFILE}" "${MONITISE_RBS_APPSTORE_PROVISONNING_PROFILE}" AppStore



#echo Making the Africa Build
#build "$RBS_TARGET_NAME" "${AFRICA_PROFILE_NAME}" "${AFRICA_SIGN_PROVISONNING_PROFILE}" "${AFRICA_PROVISONNING_PROFILE}" Africa
patchBundleVersion "$NWB_TARGET_NAME"
echo Making the MEML Build
build "$NWB_TARGET_NAME" "${MEML_PROFILE_NAME}" "${MEML_SIGN_PROVISONNING_PROFILE}" "${MEML_PROVISONNING_PROFILE}"       MEML 
echo Making the Europe Build
build "$NWB_TARGET_NAME" "${EUROPE_PROFILE_NAME}" "${EUROPE_SIGN_PROVISONNING_PROFILE}" "${EUROPE_PROVISONNING_PROFILE}" EUROPE
echo Making the Enterpise Build
build "$NWB_TARGET_NAME" "${ENTERPRISE_PROFILE_NAME}" "${ENTERPRISE_SIGN_PROVISONNING_PROFILE}" "${ENTERPRISE_PROVISONNING_PROFILE}" Enterprise

echo 'revert version changes'
git checkout "$WORKSPACE/$NWB_TARGET_NAME Branding/Info.plist"
git checkout "$WORKSPACE/$NWB_TARGET_NAME Branding/Settings.bundle/Root.plist"

echo 'revert version changes before creating of AppStore build'
echo Making the RBS AppStore Build
build "$NWB_TARGET_NAME" "${MONITISE_NWB_APPSTORE_PROFILE_NAME}" "${MONITISE_NWB_APPSTORE_SIGN_PROVISONNING_PROFILE}" "${MONITISE_NWB_APPSTORE_PROVISONNING_PROFILE}" AppStore


git reset --hard

